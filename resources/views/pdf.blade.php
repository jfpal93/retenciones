<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel="stylesheet" type="text/css" href="pdf/style.css"/>
</head>
<body>
	<img style="position:absolute;top:5.54in;left:-0.3in;width:8in;height:0.64in" src="pdf/vi_1.png" />
	<img style="position:absolute;top:0.30in;left:4.77in;width:3.94in;height:5.03in" src="pdf/vi_2.png" />
	<img style="position:absolute;top:0.30in;left:4.0in;width:3.65in;height:5.05in" src="pdf/vi_3.png" />
	<div style="position:absolute;top:0.57in;left:4.2in;width:0.65in;line-height:0.17in;"><span style="font-style:normal;font-weight:normal;font-size:12pt;font-family:Helvetica;color:#000000">R.U.C.:</span><span style="font-style:normal;font-weight:normal;font-size:12pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:1.03in;left:4.2in;width:3.38in;line-height:0.20in;"><span style="font-style:normal;font-weight:normal;font-size:14pt;font-family:Helvetica;color:#000000">COMPROBANTE DE RETENCIÓN</span><span style="font-style:normal;font-weight:normal;font-size:14pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:1.88in;left:4.2in;width:2.59in;line-height:0.17in;"><span style="font-style:normal;font-weight:normal;font-size:12pt;font-family:Helvetica;color:#000000">NÚMERO DE AUTORIZACIÓN</span><span style="font-style:normal;font-weight:normal;font-size:12pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:2.63in;left:4.2in;width:1.29in;line-height:0.13in;"><span style="font-style:normal;font-weight:normal;font-size:9pt;font-family:Helvetica;color:#000000">FECHA Y HORA DE</span><span style="font-style:normal;font-weight:normal;font-size:9pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:2.80in;left:4.2in;width:1.07in;line-height:0.13in;"><span style="font-style:normal;font-weight:normal;font-size:9pt;font-family:Helvetica;color:#000000">AUTORIZACIÓN</span><span style="font-style:normal;font-weight:normal;font-size:9pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:4.07in;left:4.2in;width:1.77in;line-height:0.17in;"><span style="font-style:normal;font-weight:normal;font-size:12pt;font-family:Helvetica;color:#000000">CLAVE DE ACCESO</span><span style="font-style:normal;font-weight:normal;font-size:12pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<img style="position:absolute;top:4.32in;left:4.2in;width:3.28in;height:0.53in" src="pdf/ri_1.png" />
	<img style="position:absolute;top:4.32in;left:4.85in;width:3.83in;height:0.76in" src="pdf/ri_2.png" />
	<img style="position:absolute;top:0.30in;left:-0.4in;width:4.32in;height:0.96in" src="pdf/ri_3.jpeg" />
	<img style="position:absolute;top:2.86in; left:-0.3in; width:4.1in;height:2.52in;" src="pdf/vi_4.png">
	<div style="position:absolute;top:2.19in;left:4.2in;width:3.75in;line-height:0.13in;"><span style="font-style:normal;font-weight:normal;font-size:9pt;font-family:Helvetica;color:#000000">{{$comprobante->infoTributaria->claveAcceso}}</span><span style="font-style:normal;font-weight:normal;font-size:9pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:2.71in;left:5.8in;width:1.73in;line-height:0.14in;"><span style="font-style:normal;font-weight:normal;font-size:10pt;font-family:Helvetica;color:#000000">{{$comprobante->infoCompRetencion->fechaEmision}}</span><span style="font-style:normal;font-weight:normal;font-size:10pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:3.65in;left:5.1in;width:0.69in;line-height:0.14in;"><span style="font-style:normal;font-weight:normal;font-size:10pt;font-family:Helvetica;color:#000000">NORMAL</span><span style="font-style:normal;font-weight:normal;font-size:10pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:0.56in;left:5.2in;width:1.59in;line-height:0.20in;"><span style="font-style:normal;font-weight:normal;font-size:14pt;font-family:Helvetica;color:#000000">{{$comprobante->infoTributaria->ruc}}</span><span style="font-style:normal;font-weight:normal;font-size:14pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:5.11in;left:4.2in;width:3.75in;line-height:0.13in;"><span style="font-style:normal;font-weight:normal;font-size:9pt;font-family:Helvetica;color:#000000">{{$comprobante->infoTributaria->claveAcceso}}</span><span style="font-style:normal;font-weight:normal;font-size:9pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:5.60in;left:-0.2in;width:2.24in;line-height:0.13in;"><span style="font-style:normal;font-weight:normal;font-size:9pt;font-family:Helvetica;color:#000000">Razón Social / Nombres y Apellidos:</span><span style="font-style:normal;font-weight:normal;font-size:9pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:5.60in;left:5.3in;width:0.87in;line-height:0.13in;"><span style="font-style:normal;font-weight:normal;font-size:9pt;font-family:Helvetica;color:#000000">Identificación:</span><span style="font-style:normal;font-weight:normal;font-size:9pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:6.03in;left:-0.2in;width:0.98in;line-height:0.13in;"><span style="font-style:normal;font-weight:normal;font-size:9pt;font-family:Helvetica;color:#000000">Fecha Emisión:</span><span style="font-style:normal;font-weight:normal;font-size:9pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:5.59in;left:2in;width:2.63in;line-height:0.14in;"><span style="font-style:normal;font-weight:normal;font-size:10pt;font-family:Helvetica;color:#000000">{{$comprobante->infoCompRetencion->razonSocialSujetoRetenido}}</span><span style="font-style:normal;font-weight:normal;font-size:10pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:5.59in;left:6.3in;width:1.14in;line-height:0.14in;"><span style="font-style:normal;font-weight:normal;font-size:10pt;font-family:Helvetica;color:#000000">{{$comprobante->infoCompRetencion->identificacionSujetoRetenido}}</span><span style="font-style:normal;font-weight:normal;font-size:10pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:6.01in;left:1.2in;width:0.72in;line-height:0.13in;"><span style="font-style:normal;font-weight:normal;font-size:9pt;font-family:Helvetica;color:#000000">{{$comprobante->infoCompRetencion->fechaEmision}}</span><span style="font-style:normal;font-weight:normal;font-size:9pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:1.47in;left:4.2in;width:0.33in;line-height:0.17in;"><span style="font-style:normal;font-weight:normal;font-size:12pt;font-family:Helvetica;color:#000000">No.</span><span style="font-style:normal;font-weight:normal;font-size:12pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:1.48in;left:5.0in;width:1.41in;line-height:0.14in;"><span style="font-style:normal;font-weight:normal;font-size:10pt;font-family:Helvetica;color:#000000">{{$comprobante->infoTributaria->estab}}-{{$comprobante->infoTributaria->ptoEmi}}-{{$comprobante->infoTributaria->secuencial}}</span><span style="font-style:normal;font-weight:normal;font-size:10pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:3.26in;left:4.2in;width:0.86in;line-height:0.14in;"><span style="font-style:normal;font-weight:normal;font-size:10pt;font-family:Helvetica;color:#000000">AMBIENTE:</span><span style="font-style:normal;font-weight:normal;font-size:10pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:3.64in;left:4.2in;width:0.72in;line-height:0.14in;"><span style="font-style:normal;font-weight:normal;font-size:10pt;font-family:Helvetica;color:#000000">EMISIÓN:</span><span style="font-style:normal;font-weight:normal;font-size:10pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:3.26in;left:5.2in;width:1.08in;line-height:0.14in;"><span style="font-style:normal;font-weight:normal;font-size:10pt;font-family:Helvetica;color:#000000">PRODUCCIÓN</span><span style="font-style:normal;font-weight:normal;font-size:10pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:3.88in;left:0.5in;width:3.22in;line-height:0.10in;"><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000">{{$comprobante->infoTributaria->dirMatriz}}</span><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:3.89in;left:-0.2in;width:0.54in;line-height:0.11in;"><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000">Dirección</span><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:4.04in;left:-0.2in;width:0.40in;line-height:0.11in;"><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000">Matriz:</span><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:5.16in;left:-0.2in;width:2.21in;line-height:0.11in;"><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000">OBLIGADO A LLEVAR CONTABILIDAD</span><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:4.50in;left:0.50in;width:3.22in;line-height:0.10in;"><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000">{{$comprobante->infoTributaria->dirMatriz}}</span><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:4.44in;left:-0.2in;width:0.54in;line-height:0.11in;"><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000">Dirección</span><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:4.59in;left:-0.2in;width:0.54in;line-height:0.11in;"><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000">Sucursal:</span><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:2.99in;left:-0.2in;width:2.39in;line-height:0.11in;"><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000">{{$comprobante->infoTributaria->razonSocial}}</span><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:5.18in;left:2.7in;width:0.13in;line-height:0.10in;"><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000">{{$comprobante->infoCompRetencion->obligadoContabilidad}}</span><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<img style="position:absolute;top:6.46in;left:-0.3in;width:8in;in;height:0.44in" src="pdf/vi_5.png" />
	<div style="position:absolute;top:6.57in;left:3.6in;width:1.15in;line-height:0.11in;"><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000">Base Imponible para</span><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:6.72in;left:3.82in;width:0.71in;line-height:0.11in;"><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000">la Retención</span><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:6.64in;left:5.12in;width:0.67in;line-height:0.11in;"><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000">IMPUESTO</span><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:6.57in;left:6.24in;width:0.61in;line-height:0.11in;"><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000">Porcentaje</span><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:6.72in;left:6.26in;width:0.58in;line-height:0.11in;"><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000">Retención</span><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<!-- <div style="position:absolute;top:6.64in;left:7.13in;width:0.84in;line-height:0.11in;"><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000">Valor Retenido</span><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div> -->
	<div style="position:absolute;top:6.57in;left:7.17in;width:1.15in;line-height:0.11in;"><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000">Valor</span><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:6.72in;left:7.10in;width:0.71in;line-height:0.11in;"><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000">Retenido</span><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<img style="position:absolute;top:6.46in;left:4.82in;width:0.02in;height:0.44in" src="pdf/vi_6.png" />
	<img style="position:absolute;top:6.46in;left:6.05in;width:0.02in;height:0.44in" src="pdf/vi_7.png" />
	<img style="position:absolute;top:6.46in;left:6.98in;width:0.02in;height:0.44in" src="pdf/vi_8.png" />
	<img style="position:absolute;top:6.46in;left:3.46in;width:0.02in;height:0.44in" src="pdf/vi_9.png" />
	<div style="position:absolute;top:6.57in;left:2.80in;width:0.49in;line-height:0.11in;"><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000">Ejercicio</span><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:6.72in;left:2.87in;width:0.35in;line-height:0.11in;"><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000">Fiscal</span><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<img style="position:absolute;top:6.46in;left:2.61in;width:0.02in;height:0.44in" src="pdf/vi_10.png" />
	<img style="position:absolute;top:6.46in;left:0.75in;width:0.02in;height:0.44in" src="pdf/vi_11.png" />
	<div style="position:absolute;top:6.64in;left:-0.16in;width:0.77in;line-height:0.11in;"><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000">Comprobante</span><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:6.64in;left:0.95in;width:0.46in;line-height:0.11in;"><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000">Número</span><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<img style="position:absolute;top:6.46in;left:1.59in;width:0.02in;height:0.44in" src="pdf/vi_12.png" />
	<div style="position:absolute;top:6.64in;left:1.67in;width:0.84in;line-height:0.11in;"><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000">Fecha Emisión</span><span style="font-style:normal;font-weight:normal;font-size:8pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<img style="position:absolute;top:6.89in;left:-0.3in;width:8in;height:0.39in" src="pdf/vi_13.png" />
	<img style="position:absolute;top:6.89in;left:4.82in;width:0.02in;height:0.39in" src="pdf/vi_14.png" />
	<img style="position:absolute;top:6.89in;left:6.05in;width:0.02in;height:0.39in" src="pdf/vi_15.png" />
	<img style="position:absolute;top:6.89in;left:6.98in;width:0.02in;height:0.39in" src="pdf/vi_16.png" />
	{!! $text !!}
	<img style="position:absolute;top:7.93in;left:-0.3in;width:5.33in;height:2.23in" src="pdf/vi_29.png" />
	<div style="position:absolute;top:8.37in;left:-0.18in;width:0.67in;line-height:0.14in;"><span style="font-style:normal;font-weight:normal;font-size:10pt;font-family:Helvetica;color:#000000">Dirección</span><span style="font-style:normal;font-weight:normal;font-size:10pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:8.37in;left:1.78in;width:2.26in;line-height:0.14in;"><span style="font-style:normal;font-weight:normal;font-size:10pt;font-family:Helvetica;color:#000000">
		@foreach ($comprobante->infoAdicional->campoAdicional as $key)
                @if($key["nombre"]=="Dirección")
                    {{$key}}
                @endif

        @endforeach
	</span><span style="font-style:normal;font-weight:normal;font-size:10pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:9.19in;left:-0.18in;width:0.63in;line-height:0.14in;"><span style="font-style:normal;font-weight:normal;font-size:10pt;font-family:Helvetica;color:#000000">Teléfono</span><span style="font-style:normal;font-weight:normal;font-size:10pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:9.19in;left:1.78in;width:0.80in;line-height:0.14in;"><span style="font-style:normal;font-weight:normal;font-size:10pt;font-family:Helvetica;color:#000000">

		@foreach ($comprobante->infoAdicional->campoAdicional as $key)
                @if($key["nombre"]=="Teléfono")
                    {{$key}}
                @endif

        @endforeach
	</span><span style="font-style:normal;font-weight:normal;font-size:10pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:9.49in;left:-0.18in;width:0.42in;line-height:0.14in;"><span style="font-style:normal;font-weight:normal;font-size:10pt;font-family:Helvetica;color:#000000">Email</span><span style="font-style:normal;font-weight:normal;font-size:10pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	<div style="position:absolute;top:9.49in;left:1.78in;width:2.06in;line-height:0.14in;"><a href="mailto:bvalle@probanaexpor.com.ec"><span style="font-style:normal;font-weight:normal;font-size:10pt;font-family:Helvetica;color:#000000">

		@foreach ($comprobante->infoAdicional->campoAdicional as $key)
                @if($key["nombre"]=="Email")
                    {{$key}}
                @endif

        @endforeach
	</span></a>
		<span style="font-style:normal;font-weight:normal;font-size:10pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
		<div style="position:absolute;top:8.02in;left:-0.18in;width:1.49in;line-height:0.14in;"><span style="font-style:normal;font-weight:normal;font-size:10pt;font-family:Helvetica;color:#000000">Información Adicional</span><span style="font-style:normal;font-weight:normal;font-size:10pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
	</body>
	</html>
