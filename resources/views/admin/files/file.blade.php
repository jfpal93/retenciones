<div class="hovereffect1 w3ls_banner_bottom_grid">


    <div class="overlay">


        <p class="modal-title">Sujeto Retenido: {{$file->razonSocialSujetoRetenido}} </p>

        <p class="modal-title">Correo: {{$file->email}} </p>

        



        <p class="modal-title">Archivo: <a href="{{url("files/files/files/$file->name")}}" target="_blank">{{$file->name}}</a> </p>

        <table class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Fecha de Emisión</th>
                <!-- <th>Sujeto Retenido</th> -->
                <th>Identificación</th>
                <th>Periodo Fiscal</th>
                @isset($file->sentDate)
                <th>Fecha de Envío</th>
                <th>Hora de Envío</th>
                @endisset
                <th>Acciones</th>


            </tr>
        </thead>
        <tbody>
            <tr>
            	<td>{{$file->fechaEmision}}</td>
            	<!-- <td>{{$file->razonSocialSujetoRetenido}}</td> -->
            	<td>{{$file->identificacionSujetoRetenido}}</td>
            	<td>{{$file->periodoFiscal}}</td>
              @isset($file->sentDate)
          	   <td>{{$file->sentDate}}</td>
               <td>{{$file->sentTime}}</td>
              @endisset
              <td>
                  <a href="{{action('FileController@streamPdf', $file->id)}}" target="_blank">
                      Ver PDF
                  </a>
              </td>
            </tr>
        </tbody>
    </table>
    </div>
</div>
