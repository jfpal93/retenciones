

<!-- Modal -->
  <div class="modal fade" id="ModalAuspicianteShow{{$file->id}}" role="dialog" style="">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Archivo</h4>
        </div>
        <div class="modal-body" style=" max-height: calc(100vh - 210px);overflow-y: auto;">
		@include("admin.files.file",["file"=>$file])
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>
