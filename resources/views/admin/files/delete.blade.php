<a href="" style="color: red; display: inline;" class="delete{{$file->id}} btn btn-link red-text no-padding no-margin no-transform">Eliminar</a>

<!-- jQuery -->
<script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>

<script type="text/javascript">
	$('.divTableCell').on('click', '.delete{{$file->id}}', function() {
		 $.ajax({
				 type: 'DELETE',
				 url: 'files/' + {{$file->id}},
				 data: {
						 '_token': $('input[name=_token]').val(),
				 },
				 success: function(data) {
						 $('.row' + data['id']).remove();
				 }
		 });
 });
</script>
