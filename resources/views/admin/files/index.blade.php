


<div class="row">
        <div class="col-md-12">

            <div class="white-box">
		            <div class="comment-body">

		            	<h3>Archivos No enviados
		            		<!-- <div class="floating"> -->
								<a class="btn btn-primary btn-fab" href="" data-toggle="modal" onclick="emptyModalNewAuspiciante()" data-target="#ModalAuspicianteCreate">
									<i class="material-icons">Agregar</i>
								</a >
							<!-- </div> -->
						</h3>
						{!! Form::open(array('route' => 'sendmail','name'=>'sendMailButton')) !!}


							<div class="table-responsive">
								<div class="divTable">
									<div class="divTableBody">
										<div class="divTableRow">
											<div class="divTableCell">Selección</div>
                      <div class="divTableCell">No. Retención</div>
                      <div class="divTableCell">No. documento</div>
											<div class="divTableCell">Identificación</div>
											<div class="divTableCell">Razón Social Sujeto Retenido</div>
											<div class="divTableCell">Ingreso</div>

										</div>

											@isset($filesNotSent)

											    @foreach ($filesNotSent as $file)
													<div id="rowNotSent" class="divTableRow row{{$file->id}}">

														<div class="divTableCell" id="sel">

															{!! Form::checkbox('selection[]',$file->id,null,['onchange'=>'isChecked(this)']) !!}
														</div>
                            <div class="divTableCell" id="nDoc">{{$file->estab}}-{{$file->ptoEmi}}-{{$file->secuencial}}</div>

                            <div class="divTableCell" id="numDocSustento">{{$file->numdocsutento}}</div>

														<div class="divTableCell" id="id">{{$file->identificacionSujetoRetenido}}</div>
														<div class="divTableCell" id="client">{{$file->razonSocialSujetoRetenido}}</div>
														<div class="divTableCell" id="dh">{{$file->uploadDate}} - {{$file->uploadTime}}</div>



														<div class="divTableCell" id="">
															<a href="" data-toggle="modal" data-target="#ModalAuspicianteShow{{$file->id}}">Ver</a >
                              @include('admin.files.delete',['file'=>$file])
														</div>



													</div>
												@endforeach



											@endisset

									</div>
								</div>

							</div>
							<input id="sendMail" name="sendMailButton" type="submit" value="Enviar" class="click btn btn-success">

						{!! Form::close() !!}


					</div>



            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

            <div class="white-box">
		            <div class="comment-body">
		            	<h3>Archivos enviados</h3>
		            		{!! Form::open(array('route' => 'reSendMail')) !!}
							<div class="table-responsive">
								<div class="divTable">
									<div class="divTableBody">
										<div class="divTableRow">
											<div class="divTableCell">Selección</div>
                      <div class="divTableCell">No. Retención</div>
                      <div class="divTableCell">No. documento</div>
											<div class="divTableCell">Identificación</div>
											<div class="divTableCell">Razón Social Sujeto Retenido</div>

											<div class="divTableCell">Ultimo envio</div>
										</div>

											@isset($filesSent)

											    @foreach ($filesSent as $file)
													<div id="rowSent" class="divTableRow">
														<div class="divTableCell" id="sel">

															{!! Form::checkbox('selection2[]',$file->id,null,['onchange'=>'isChecked2(this)']) !!}
														</div>
                            <div class="divTableCell" id="nDoc">{{$file->estab}}-{{$file->ptoEmi}}-{{$file->secuencial}}</div>
                            <div class="divTableCell" id="numDocSustento">{{$file->numdocsutento}}</div>
                            <div class="divTableCell" id="id">{{$file->identificacionSujetoRetenido}}</div>
														<div class="divTableCell" id="client">{{$file->razonSocialSujetoRetenido}}</div>

														@if(isset($file->reSentDate))
															<div class="divTableCell" id="dh">{{$file->reSentDate}} - {{$file->reSentTime}}</div>

														@else
															<div class="divTableCell" id="dh">{{$file->sentDate}} - {{$file->sentTime}}</div>

														@endif
														<div class="divTableCell" id="">
															<a href="" data-toggle="modal" data-target="#ModalAuspicianteShow{{$file->id}}">Ver</a >





														</div>



													</div>
												@endforeach



											@endisset

									</div>
								</div>

							</div>
							<input id="reSendMail" type="submit" value="Enviar" class="click btn btn-success">

						{!! Form::close() !!}

					</div>



            </div>
        </div>
    </div>

    @include("admin.files.create",["filesNotSent"=>$filesNotSent])

    @isset($filesNotSent)
    	@foreach ($filesNotSent as $file)
    		@include("admin.files.show",["file"=>$file])
		@endforeach
	@endisset

	@isset($filesNotSent)
    	@foreach ($filesSent as $file)
    		@include("admin.files.show",["file"=>$file])
		@endforeach
	@endisset

    <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript">
    	$("#sendMail").attr('disabled', 'disabled');
    	function isChecked(checkbox) {
		  // checkbox
		  // alert('yeii');
		  var chkArray = [];

			/* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
			$('input[name="selection[]"]:checked').each(function() {
				chkArray.push($(this).val());
			});

			/* we join the array separated by the comma */
			var selected;
			selected = chkArray.join(',') ;

			/* check if there is selected checkboxes, by default the length is 1 as it contains one single comma */
			if(selected.length > 0){
				$("#sendMail").removeAttr('disabled');
			}else{
				$("#sendMail").attr('disabled', 'disabled');
			}
		}

		$("#reSendMail").attr('disabled', 'disabled');
    	function isChecked2(checkbox) {
		  // checkbox
		  // alert('yeii');
		  var chkArray = [];

			/* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
			$('input[name="selection2[]"]:checked').each(function() {
				chkArray.push($(this).val());
			});

			/* we join the array separated by the comma */
			var selected;
			selected = chkArray.join(',') ;

			/* check if there is selected checkboxes, by default the length is 1 as it contains one single comma */
			if(selected.length > 0){
				$("#reSendMail").removeAttr('disabled');
			}else{
				$("#reSendMail").attr('disabled', 'disabled');
			}
		}


		function GetElementInsideContainer(containerID, childID) {
		    var elm = {};
		    var elms = document.getElementById(containerID).getElementsByTagName("*");
		    for (var i = 0; i < elms.length; i++) {
		        if (elms[i].id === childID) {
		            elm = elms[i];
		            break;
		        }
		    }
		    return elm;
		}
		function emptyModalNewAuspiciante(){
			var e = GetElementInsideContainer("ModalAuspicianteCreate", "fileName");
			e.value="";

		}



		$('#searchPerson').on('input',function(e){
		    var input, filter, ul, li, a, i,filter2;
			    input = document.getElementById("searchPerson");

			var input2 = $.extend( true, {}, input );
			// var input2 = $.extend( true, {}, input );

			    filter = input.value.toUpperCase();
			    filter2=input2.value.toUpperCase();
			    // // }

			    var ind=[];
			    var bool=false;


			    if (!isNaN(parseInt(filter))){
			    	var notSent = document.querySelectorAll("#rowNotSent");
		    		var sent = document.querySelectorAll("#rowSent");

		    		for (i = 0; i < notSent.length; i++) {
				    	var idC = notSent[i].childNodes;
					    		if(idC[5].innerHTML.toUpperCase().indexOf(filter) > -1){
					    			notSent[i].style.display = "table-row";
					    		}

				    		 	else {
				    		 		if(idC[3].innerHTML.toUpperCase().indexOf(filter) > -1)
				    		 		{
										notSent[i].style.display = "table-row";
				    		 		}
				    		 		else{
				    		 			notSent[i].style.display = "none";
				    		 		}

					            }
			    	}

			    	for (i = 0; i < sent.length; i++) {
				    	var idC = sent[i].childNodes;
			    		if(idC[5].innerHTML.toUpperCase().indexOf(filter) > -1){
			    			sent[i].style.display = "table-row";
			    		}

		    		 	else {
		    		 		if(idC[3].innerHTML.toUpperCase().indexOf(filter) > -1)
		    		 		{
								sent[i].style.display = "table-row";
		    		 		}
		    		 		else{
		    		 			sent[i].style.display = "none";
		    		 		}

			            }
			    	}


			    }
			    else{

			    	filterWordOrNUmber(filter,"client");
			    }







		});

		function filterWordOrNUmber(filter,type){
			 var notSent = document.querySelectorAll("#rowNotSent");
		    var sent = document.querySelectorAll("#rowSent");

		    	filterFunction(notSent,type,filter);
				filterFunction(sent,type,filter);


		}
		function filterFunction(array,type,filter){
			for (i = 0; i < array.length; i++) {
		    	var idC = array[i].childNodes;
		    	for(j=0;j<idC.length;j++){
		    		if(idC[j].id == type){
			    		// alert(idC[j].innerHTML);
			    		if(idC[j].innerHTML.toUpperCase().indexOf(filter) > -1){
			    			array[i].style.display = "table-row";
			    		}

		    		 	else {
			            	array[i].style.display = "none";
			            }
			    	}

		        }
	    	}
		}

		function filterWordOrNUmber2(filter,type){
			 var notSent = document.querySelectorAll("#rowNotSent");
		    var sent = document.querySelectorAll("#rowSent");

		    	filterFunction2(notSent,type,filter);
				filterFunction2(sent,type,filter);


		}
		function filterFunction2(array,type,filter){
			for (i = 0; i < array.length; i++) {
		    	var idC = array[i].childNodes;
		    	for(j=0;j<idC.length;j++){
		    		if(idC[j].id === type){
			    		// alert(idC[j].innerHTML);
			    		if(idC[j].innerHTML.toUpperCase().indexOf(filter) > -1){
			    			array[i].style.display = "table-row";
			    		}

		    		 	else {
			            	array[i].style.display = "none";
			            }
			    	}

		        }
	    	}
		}




	</script>
