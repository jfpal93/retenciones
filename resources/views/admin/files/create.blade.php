
<!-- Modal -->
  <div class="modal fade" id="ModalAuspicianteCreate" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Nuevo Archivo</h4>
        </div>
        <div class="modal-body">
          @if (count($errors) > 0)
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          @endif
		      @include("admin.files.form",['url' => '/files','method'=>'POST'])
         </div>
        
         	
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>