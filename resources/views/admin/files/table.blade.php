@isset($filesNotSent)

    @foreach ($filesNotSent as $file)
    <div id="rowNotSent" class="divTableRow">

      <div class="divTableCell" id="sel">

        {!! Form::checkbox('selection[]',$file->id,null,['onchange'=>'isChecked(this)']) !!}
      </div>
      <div class="divTableCell" id="nDoc">{{$file->estab}}-{{$file->ptoEmi}}-{{$file->secuencial}}</div>

      <div class="divTableCell" id="id">{{$file->identificacionSujetoRetenido}}</div>
      <div class="divTableCell" id="client">{{$file->razonSocialSujetoRetenido}}</div>
      <div class="divTableCell" id="dh">{{$file->uploadDate}} - {{$file->uploadTime}}</div>



      <div class="divTableCell" id="">
        <a href="" data-toggle="modal" data-target="#ModalAuspicianteShow{{$file->id}}">Ver</a >
          <?php echo $file->id ?>
        @include('admin.files.delete',['file'=>$file])
      </div>



    </div>
  @endforeach



@endisset
