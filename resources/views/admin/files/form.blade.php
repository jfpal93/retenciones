<!--FOrmulario-->

{!! Form::open(['url'=>$url,'method'=>$method, 'files'=>true]) !!} 
{!! csrf_field() !!}
	<div class="form-group">
		{{ Form::file('file[]',['required' => 'required','type'=>'file','multiple'=>true])}}
	</div>
	
	
	<div class="form-group text-right">
		<!-- <a href="{{url('/home')}}">Regresar</a> -->

		<input type="submit" value="Enviar" class="btn btn-success">
	</div>

{!! Form::close() !!}