@extends('layouts.app2')

@section('content')
<!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="fa fa-bars"></i></a>
                <div class="top-left-part"><a class="logo" style="margin-left: 16px;"><b>Hacienda</b><span class="hidden-xs" style="    margin-left: 34px;">      Rosita</span></a></div>
                <ul class="nav navbar-top-links navbar-left m-l-20 hidden-xs">
                    <li>
                        <form role="search" class="app-search hidden-xs">
                            <input id="searchPerson" type="text" placeholder="Buscar..." class="form-control"> <a href=""><i class="fa fa-search"></i></a>
                        </form>
                    </li>
                </ul>
                
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li>
                    @if (Auth::guest())
                       
                    @else
                        <a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out  fa-fw" aria-hidden="true"></i><span class="hide-menu">Logout</span>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {!! csrf_field() !!}
                            </form>
                    @endif
                        
                    </li>
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <!-- Left navbar-header -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse slimscrollsidebar">
                <ul class="nav" id="side-menu">
                    <li style="padding: 10px 0 0;">
                        <a href="{{url('/home')}}" class="waves-effect"><i class="fa fa-home fa-fw" aria-hidden="true"></i><span class="hide-menu">Inicio</span></a>
                    </li>
                    <li>
                        <a href="{{url('/history')}}" class="waves-effect"><i class="fa fa-history fa-fw" aria-hidden="true"></i><span class="hide-menu">Historial</span></a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Left navbar-header end -->
        <!-- Page Content -->
        <div id="page-wrapper">
           

            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Index</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><i class="fa fa-fort-awesome"></i>ESPINOSA VALVERDE JORGE FABRICIO - 0702033002001</li>
                        </ol>
                    </div>
                </div>
                @include("admin.files.index",["filesNotSent"=>$filesNotSent,'filesSent'=>$filesSent])
                
                
            </div>

            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; hcdarosita.com</footer>





        </div>
        <!-- /#page-wrapper -->
<script>
    var msg = '{{Session::get('alert')}}';
    var exist = '{{Session::has('alert')}}';
    if(exist){
      alert(msg);
    }
</script>
@endsection

