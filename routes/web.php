<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();
use App\Mail\SendFiles;

Route::get('/', 'MainController@home');

Route::get('/home', 'HomeController@index');

Route::get('/history', 'HistorialController@historial');

Route::resource('files','FileController');

// Route::post('files/sendmail', array('as' => 'sendmail', 'uses' => 'FileController@sendMail'));

// Route::get('sendmail', 'SendMailController@sendMail');

// Route::post('sendmail', 'SendMailController@sendMail');

// Route::post('sendmail', array('as' => 'sendmail','uses'=>'SendMailController@sendMail'));


Route::get('send',
  ['as' => 'send', 'uses' => 'SendMailController@home']);
Route::post('send',
  ['as' => 'sendmail', 'uses' => 'SendMailController@sendMail']);

Route::get('resend',
  ['as' => 'resend', 'uses' => 'SendMailController@home']);
Route::post('resend',
  ['as' => 'reSendMail', 'uses' => 'SendMailController@reSendMail']);
// Route::get('/upload', 'UploadController@uploadForm');
// Route::post('/upload', 'UploadController@uploadSubmit');

Route::get('/pdf/{id}','FileController@streamPdf');

Route::get('files/files/files/{filename}',function($filename){
	$path=storage_path("app/files/$filename");

	if(!\File::exists($path)){
		abort(404);
	}

	$file = \File::get($path);

	$type = \File::mimeType($path);

	$response = Response::make($file,200);

	$response->header("Content-Type",$type);

	return $response;
});

Route::get('pdfFile/{filename}',function($filename){
	$path=storage_path("app/$filename");

	if(!\File::exists($path)){
		abort(404);
	}

	$file = \File::get($path);

	$type = \File::mimeType($path);

	$response = Response::make($file,200);

	$response->header("Content-Type",$type);

	return $response;
});
