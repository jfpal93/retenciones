<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('files',function($table){
          $table->string('name')->unique()->change();
          $table->string('fechaEmision');
          $table->string('razonSocialSujetoRetenido');
          $table->string('identificacionSujetoRetenido');
          $table->string('periodoFiscal');
          $table->string('email');
          $table->string('estab');
          $table->string('ptoEmi');
          $table->string('secuencial');
          $table->boolean('sent');
          $table->string('extension')->nullable();
            
            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
