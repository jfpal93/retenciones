<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendFiles extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // return $this->from("noreply@machincorp.com")->view('mailers.contact');
        $address = 'docelec@hcdarosita.com';
 
      $name = 'Página web';
 
      $subject = 'Contacto desde formulario';
 
      return $this->view('mailers.contact')
 
      ->from($address, $name)
 
      ->subject($subject);
    }
}
