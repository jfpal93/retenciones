<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\File;
use Storage;
use App\Http\Requests\UploadRequest;
use XmlParser;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use \Illuminate\Support\Str;
use PDF;


use Carbon\Carbon;

class FileController extends Controller
{
    //
    //
    public function __construct(){
        $this->middleware("auth");
    }

    public function index()
    {
        //Muestra la coleccion de elementos
        $filesNotSent = DB::table('files')
        ->orderBy('created_at', 'DESC')
        ->where('sent', FALSE)->get();

            $filesSent = DB::table('files')
            ->where('sent', TRUE)->get();

        // if($files){
            return view("admin.files.index",["filesNotSent"=>$filesNotSent,"filesSent"=>$filesSent]);
        // }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Despliega la vista para crear el nuevo elemento
        $file = new File;
        return view("admin.files.create",["file"=>$file]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $Files = $request->file;

        $destinationPath = public_path('files');


        // echo $now->toDateString();
        // echo $now->toTimeString();
        // echo $destinationPath;

        foreach($Files as $file){
            // $extension =  $file->extension();
            $fileName = $file->getClientOriginalName();
            // $path=$file->storeAs('files',$fileName);

            // $xml = simplexml_load_file($file);
            // // if($xml){
            // //     echo $xml->autorizacion->estado;
            // // }

            // echo $xml->autorizacion[0];
            $comprobante = simplexml_load_file($file);
            // printf("<p>%s appeared in </p>",$comprobante->infoCompRetencion->razonSocialSujetoRetenido);

            // echo $comprobante->infoAdicional->campoAdicional[1];


            $now = Carbon::now('America/Bogota');
            $archive = new File;
            $archive->name=$fileName;
            if($comprobante->infoTributaria->ruc == '0702033002001'){
                $archive->fechaEmision=$comprobante->infoCompRetencion->fechaEmision;
                $archive->razonSocialSujetoRetenido=$comprobante->infoCompRetencion->razonSocialSujetoRetenido;
                $archive->identificacionSujetoRetenido=$comprobante->infoCompRetencion->identificacionSujetoRetenido;
                $archive->periodoFiscal=$comprobante->infoCompRetencion->periodoFiscal;
                $archive->sentTimes=0;
                foreach ($comprobante->infoAdicional->campoAdicional as $key) {
                    if($key["nombre"]=="Email"){
                        $archive->email=$key;
                    }
                }
                // $archive->campoAdicional=$comprobante->infoAdicional->campoAdicional["nombre"];
                $archive->estab=$comprobante->infoTributaria->estab;
                $archive->ptoEmi=$comprobante->infoTributaria->ptoEmi;
                $archive->secuencial=$comprobante->infoTributaria->secuencial;
                $archive->sent=FALSE;
                $archive->uploadDate=$now->toDateString();
                $archive->uploadTime=$now->toTimeString();
                $archive->extension=$file->extension();
                $archive->numdocsutento=$comprobante->impuestos->impuesto->numDocSustento;
                $archive->user_id = Auth::id();
                try{
                    if($archive->save()){
                        $file->storeAs('files',$fileName);
                    }
                    else{
                        return view("admin.files.create",["archive"=>$archive]);

                    }
                }
                catch(\Illuminate\Database\QueryException $e){
                    $errorCode = $e->errorInfo[1];
                    if($errorCode == '1062'){
                        // return redirect("/home");
                    }
                }
                return redirect("/home");
            }
            else{
                return redirect("/home")->with('alert', 'RUC Invalido!!');
            }
            

            // $archive->fechaEmision=$comprobante->infoCompRetencion->fechaEmision;
            // $archive->fechaEmision=$comprobante->infoCompRetencion->fechaEmision;
            // $archive->fechaEmision=$comprobante->infoCompRetencion->fechaEmision;

        // $file->name =
        // $file->description = $request->description;
        // $file->descripcion_corta = $request->short;
        // $file->user_id = Auth::id();

            // foreach ($xml->infoTributaria as $lang) {
            //     # code...
            //     echo $lang["name"];
            //     // printf(
            //     //     "<p>%s appeared in %d and was created by %s.</p>",
            //     //     $lang["name"],
            //     //     $lang->appeared,
            //     //     $lang->creator
            //     // );
            // }

            // // echo $path;
            // $path2=Storage::disk('local')->url($fileName);
            // echo $path2;

            // $fullPath='files'.$path2;
            // // echo $fullPath;
            // // echo $extension;
            // // echo $fileName;
            // // $content = Storage::get($fileName);
            // $xml = \XmlParser::extract("files/files/files/$fileName");
            // if($xml){
            //     echo "yes!!";

            // }
        }
        

        // if($hasFiles && ){
        //     echo "yes!!";
        // $files = $request->file;
        // $file_count = count($files);
        // foreach($files as $file) {
        //        // $validator=Validator::make($files, [
        //        //      'application' => 'mimes:xml' //only allow this type extension file.
        //        //  ]);
        //        // if($validator){
        //        //  echo "eureka!!";
        //        // }

        //         //
        //     // }
        //     $this->validate($file, [
        //         'file' => 'mimes:xml' //only allow this type extension file.
        //     ]);



        // }

        // $files = $request->hasFile('file');

        // $this->validate()



        // //Guarda el nuevo elemento
        // $file = new File;
        // $file->name = $request->name;
        // $file->description = $request->description;
        // $file->descripcion_corta = $request->short;
        // $file->user_id = Auth::id();

        // if($hasFile){
        //     $extension =  $request->cover->extension();
        //     $file->extension = $extension;
        // }
        // if($hasFile2){
        //     $extension2 =  $request->cover2->extension();
        //     $file->extension2 = $extension2;
        // }
        // if($hasFile3){
        //     $extension3 =  $request->cover3->extension();
        //     $file->extension3 = $extension3;
        // }

        // if($file->save()){
        //     if($hasFile){
        //         $request->cover->storeAs('images/files',"principal-$file->id.$extension");
        //     }
        //     if($hasFile2){
        //         $request->cover2->storeAs('images/files',"modal1-$file->id.$extension2");
        //     }
        //     if($hasFile3){
        //         $request->cover3->storeAs('images/files',"modal2-$file->id.$extension3");
        //     }
        //     return redirect("/home");
        // }else{
        //     return view("admin.files.create",["file"=>$file]);

        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Muestra el invitadoo
        $file = File::find($id);
        return view('admin.files.show',['file'=>$file]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Edita el invitadoo
        $file = File::find($id);
        return view("admin.files.edit",["file"=>$file]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $hasFile = $request->hasFile('cover') && $request->cover->isValid();
        $hasFile2 = $request->hasFile('cover2') && $request->cover2->isValid();
        $hasFile3 = $request->hasFile('cover3') && $request->cover3->isValid();
        //Actualiza lo editado
        $file = File::find($id);
        $file->name = $request->name;
        $file->description = $request->description;
        $file->descripcion_corta = $request->short;
        $file->user_id = Auth::id();

        if($hasFile){
            $extension =  $request->cover->extension();
            $file->extension = $extension;
        }
        if($hasFile2){
            $extension2 =  $request->cover2->extension();
            $file->extension2 = $extension2;
        }
        if($hasFile3){
            $extension3 =  $request->cover3->extension();
            $file->extension3 = $extension3;
        }

        if($file->save()){
            if($hasFile){
                $request->cover->storeAs('images/files',"principal-$file->id.$extension");
            }
            if($hasFile2){
                $request->cover2->storeAs('images/files',"modal1-$file->id.$extension2");
            }
            if($hasFile3){
                $request->cover3->storeAs('images/files',"modal2-$file->id.$extension3");
            }
            return redirect("/home");
        }else{
            return view("admin.files.edit",["file"=>$file]);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Elimina el elemento
        //$file = File::find($id);
        File::destroy($id);

        return redirect('/home');
    }

    public function streamPdf($id){
      $correo="";
      $file = DB::table('files')->where('id', $id)->first();
      $name=$file->name;
      $razon =$file->razonSocialSujetoRetenido;
      $correo .= $file->email;

      $destinationPath = public_path('files');
      $pdfDestinationPath = public_path('pdf');
      // echo $destinationPath;

      $pathFile="https://hcdarosita.com/files/files/files/" . $name;

      // $pdf = PDF::loadView('pdf', compact('file'));
      $comprobante = simplexml_load_file($pathFile);

      $emisor=$comprobante->infoTributaria->razonSocial;

      $fecha=$file->fechaEmision;


      if($comprobante){
          $text='';
          if( count($comprobante->impuestos->impuesto)>=1)
          {
              $temp =1;
              $temp2 =0;
              $temp3 =0;
              foreach ($comprobante->impuestos->impuesto as $imp)
              {



                  $var = 6.89;
                  $var2 = 0.38;

                  $pad1 = 7.05;
                  $pad2=0.38;

                  $tagProp = $var + ($var2 * $temp);
                  $tagProp2 = $pad1 + ($pad2 * $temp2);

                  $var31 = 6.89;
                  $var32 = 0.38;
                  $tagProp3 = $var31 + ($var32 * $temp3);


                  $text .='<div style="position:absolute;top:'.$tagProp2.'in;left:4.06in;width:0.21in;line-height:0.10in;"><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000">'. $imp->baseImponible . '</span><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
                      <div style="position:absolute;top:'.$tagProp2.'in;left:6.46in;width:0.18in;line-height:0.10in;"><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000">'. $imp->porcentajeRetener .'</span><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
                      <div style="position:absolute;top:'.$tagProp2.'in;left:7.2in;width:0.24in;line-height:0.10in;"><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000">'. $imp->valorRetenido .'</span><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
                      <div style="position:absolute;top:'.$tagProp2.'in;left:5.29in;width:0.39in;line-height:0.10in;"><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000">';
                  if($imp->codigo == 1){
                      $text .= 'RENTA';
                  }
                  else{
                      $text .= 'IVA';
                  }

                  $text .= '</span><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
                      <img style="position:absolute;top:'.$tagProp3.'in;left:3.46in;width:0.02in;height:0.39in" src="pdf/vi_17.png" />
                      <img style="position:absolute;top:'.$tagProp3.'in;left:2.61in;width:0.02in;height:0.39in" src="pdf/vi_18.png" />
                      <img style="position:absolute;top:'.$tagProp3.'in;left:0.75in;width:0.02in;height:0.39in" src="pdf/vi_19.png" />
                      <div style="position:absolute;top:'.$tagProp2.'in;left:-0.16in;width:0.53in;line-height:0.10in;"><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000">FACTURA</span><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
                      <div style="position:absolute;top:'.$tagProp2.'in;left:0.78in;width:0.80in;line-height:0.10in;"><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000">'. $imp->numDocSustento .'</span><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
                      <div style="position:absolute;top:'.$tagProp2.'in;left:2.87in;width:0.41in;line-height:0.10in;"><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000">'.$comprobante->infoCompRetencion->periodoFiscal.'</span><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
                      <img style="position:absolute;top:'.$tagProp3.'in;left:1.59in;width:0.02in;height:0.39in" src="pdf/vi_20.png" />
                      <div style="position:absolute;top:'.$tagProp2.'in;left:1.7in;width:0.56in;line-height:0.10in;"><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000">'. $imp->fechaEmisionDocSustento .'</span><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>';
                  if((count($comprobante->impuestos->impuesto)-$temp)>0)
                  {
                      $text .= '<img  style="position:absolute;top:'.$tagProp.'in;left:-0.3in;width:8in;height:0.39in" src="pdf/vi_21.png" />
                      <img style="position:absolute;top:'.$tagProp.'in;left:4.82in;width:0.02in;height:0.39in" src="pdf/vi_22.png" />
                      <img style="position:absolute;top:'.$tagProp.'in;left:6.05in;width:0.02in;height:0.39in" src="pdf/vi_23.png" />
                      <img style="position:absolute;top:'.$tagProp.'in;left:6.98in;width:0.02in;height:0.39in" src="pdf/vi_24.png" />';
                  }
                  $temp=$temp+1;
                  $temp2=$temp2+1;
                  $temp3=$temp3+1;
              }
          }

          $pdfFileName =str_replace(".xml",".pdf",$name);
          $pdfPath=$pdfDestinationPath . $pdfFileName;
          $pdf = PDF::loadView( 'pdf', compact('comprobante','text'))->setPaper('a4', 'portrait');
          return $pdf->stream("$pdfFileName");

      }


    }


}
