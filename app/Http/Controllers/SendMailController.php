<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use Illuminate\Support\Facades\DB;
use App\File;
use PDF;
use XmlParser;
use Storage;
use Carbon\Carbon;

use CodeItNow\BarcodeBundle\Utils\BarcodeGenerator;

use App\Mail\SendFiles;

class SendMailController extends Controller
{
    //use App\File;
// $files = File::All();

    public function home(){

	    }

    public function sendMail(Request $request)
    {
      $correo="";
    	// echo "Send!!";
    	$ids = $request->selection;
    	foreach ($ids as $id) {
    		# code...
            $file = DB::table('files')->where('id', $id)->first();
    		$name=$file->name;
            $razon =$file->razonSocialSujetoRetenido;
            $correo .= $file->email;
            // $request->file('attachment')->getRealPath()

            // $path=file("{{url("files/files/files/$file->name")}}")->getRealPath();
            // $path = file($name);
            $destinationPath = public_path('files');
            $pdfDestinationPath = public_path('pdf');
            // echo $destinationPath;

            $pathFile="https://hcdarosita.com/files/files/files/" . $name;

            // $pdf = PDF::loadView('pdf', compact('file'));
            $comprobante = simplexml_load_file($pathFile);

            $emisor=$comprobante->infoTributaria->razonSocial;

            $fecha=$file->fechaEmision;


            if($comprobante){
                $text='';
                if( count($comprobante->impuestos->impuesto)>=1)
                {
                    $temp =1;
                    $temp2 =0;
                    $temp3 =0;
                    foreach ($comprobante->impuestos->impuesto as $imp)
                    {



                        $var = 6.89;
                        $var2 = 0.38;

                        $pad1 = 7.05;
                        $pad2=0.38;

                        $tagProp = $var + ($var2 * $temp);
                        $tagProp2 = $pad1 + ($pad2 * $temp2);

                        $var31 = 6.89;
                        $var32 = 0.38;
                        $tagProp3 = $var31 + ($var32 * $temp3);


                        $text .='<div style="position:absolute;top:'.$tagProp2.'in;left:4.06in;width:0.21in;line-height:0.10in;"><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000">'. $imp->baseImponible . '</span><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
                            <div style="position:absolute;top:'.$tagProp2.'in;left:6.46in;width:0.18in;line-height:0.10in;"><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000">'. $imp->porcentajeRetener .'</span><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
                            <div style="position:absolute;top:'.$tagProp2.'in;left:7.2in;width:0.24in;line-height:0.10in;"><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000">'. $imp->valorRetenido .'</span><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
                            <div style="position:absolute;top:'.$tagProp2.'in;left:5.29in;width:0.39in;line-height:0.10in;"><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000">';
                        if($imp->codigo == 1){
                            $text .= 'RENTA';
                        }
                        else{
                            $text .= 'IVA';
                        }

                        $text .= '</span><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
                            <img style="position:absolute;top:'.$tagProp3.'in;left:3.46in;width:0.02in;height:0.39in" src="pdf/vi_17.png" />
                            <img style="position:absolute;top:'.$tagProp3.'in;left:2.61in;width:0.02in;height:0.39in" src="pdf/vi_18.png" />
                            <img style="position:absolute;top:'.$tagProp3.'in;left:0.75in;width:0.02in;height:0.39in" src="pdf/vi_19.png" />
                            <div style="position:absolute;top:'.$tagProp2.'in;left:-0.16in;width:0.53in;line-height:0.10in;"><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000">FACTURA</span><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
                            <div style="position:absolute;top:'.$tagProp2.'in;left:0.78in;width:0.80in;line-height:0.10in;"><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000">'. $imp->numDocSustento .'</span><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
                            <div style="position:absolute;top:'.$tagProp2.'in;left:2.87in;width:0.41in;line-height:0.10in;"><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000">'.$comprobante->infoCompRetencion->periodoFiscal.'</span><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
                            <img style="position:absolute;top:'.$tagProp3.'in;left:1.59in;width:0.02in;height:0.39in" src="pdf/vi_20.png" />
                            <div style="position:absolute;top:'.$tagProp2.'in;left:1.7in;width:0.56in;line-height:0.10in;"><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000">'. $imp->fechaEmisionDocSustento .'</span><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>';
                        if((count($comprobante->impuestos->impuesto)-$temp)>0){
                            $text .= '<img  style="position:absolute;top:'.$tagProp.'in;left:-0.3in;width:8in;height:0.39in" src="pdf/vi_21.png" />
                            <img style="position:absolute;top:'.$tagProp.'in;left:4.82in;width:0.02in;height:0.39in" src="pdf/vi_22.png" />
                            <img style="position:absolute;top:'.$tagProp.'in;left:6.05in;width:0.02in;height:0.39in" src="pdf/vi_23.png" />
                            <img style="position:absolute;top:'.$tagProp.'in;left:6.98in;width:0.02in;height:0.39in" src="pdf/vi_24.png" />';
                        }
                        $temp=$temp+1;
                        $temp2=$temp2+1;
                        $temp3=$temp3+1;
                    }
                }

                $pdfFileName =str_replace(".xml",".pdf",$name);
                $pdfPath=$pdfDestinationPath . $pdfFileName;
                $pdf = PDF::loadView( 'pdf', compact('comprobante','text'))->setPaper('a4', 'portrait');

                Storage::put($pdfFileName, $pdf->output());
            }
            $pdfPathFile = "https://hcdarosita.com/pdfFile/" . $pdfFileName;

            $correoCC="";

            if(is_null($correo)){
                $correoCC="jorgeespinosaretenciones@gmail.com";
                $correo="jorgeespinosaretenciones@gmail.com";
            }
            else{
                $correoCC="jorgeespinosaretenciones@gmail.com";
            }

            Mail::send('sendmail',
            array(
                'name' => $name,
                'email' => $correo,
                'razon' => $razon,
                'fechaEmision' => $fecha,
                'emisor' => $emisor
            ), function($message) use ($pdfPathFile, $razon,$pathFile,$correoCC,$correo)
              {
               $message->from('docelec@hcdarosita.com','Hacienda Rosita');
               $message->to($correo, "$razon")->subject("Retención $razon");
               $message->cc($correoCC);
               $message->attach($pdfPathFile);
               $message->attach($pathFile);
           });

            Storage::delete($pdfFileName);
            $now = Carbon::now('America/Bogota');

            DB::table('files')
            ->where('id', $id)
            ->update(['sent' => TRUE]);

            DB::table('files')
            ->where('id', $id)
            ->update(['sentDate' => $now->toDateString()]);

            DB::table('files')
            ->where('id', $id)
            ->update(['sentTime' => $now->toTimeString()]);
    	}
        return redirect("/home");
    }

    public function reSendMail(Request $request)
    {
      $correo="";
        // echo "Send!!";
        $ids = $request->selection2;
        foreach ($ids as $id) {
            # code...
            $file = DB::table('files')->where('id', $id)->first();
            $name=$file->name;
            $razon =$file->razonSocialSujetoRetenido;
            $correo .= $file->email;

            $destinationPath = public_path('files');
            $pdfDestinationPath = public_path('pdf');

            $pathFile="https://hcdarosita.com/files/files/files/" . $name;

            $comprobante = simplexml_load_file($pathFile);

            $emisor=$comprobante->infoTributaria->razonSocial;

            $fecha=$file->fechaEmision;


            if($comprobante){
                $text='';
                if( count($comprobante->impuestos->impuesto)>=1)
                {
                    $temp =1;
                    $temp2 =0;
                    $temp3 =0;
                    foreach ($comprobante->impuestos->impuesto as $imp)
                    {



                        $var = 6.89;
                        $var2 = 0.38;

                        $pad1 = 7.05;
                        $pad2=0.38;

                        $tagProp = $var + ($var2 * $temp);
                        $tagProp2 = $pad1 + ($pad2 * $temp2);

                        $var31 = 6.89;
                        $var32 = 0.38;
                        $tagProp3 = $var31 + ($var32 * $temp3);


                        $text .='<div style="position:absolute;top:'.$tagProp2.'in;left:4.06in;width:0.21in;line-height:0.10in;"><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000">'. $imp->baseImponible . '</span><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
                            <div style="position:absolute;top:'.$tagProp2.'in;left:6.46in;width:0.18in;line-height:0.10in;"><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000">'. $imp->porcentajeRetener .'</span><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
                            <div style="position:absolute;top:'.$tagProp2.'in;left:7.2in;width:0.24in;line-height:0.10in;"><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000">'. $imp->valorRetenido .'</span><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
                            <div style="position:absolute;top:'.$tagProp2.'in;left:5.29in;width:0.39in;line-height:0.10in;"><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000">';
                        if($imp->codigo == 1){
                            $text .= 'RENTA';
                        }
                        else{
                            $text .= 'IVA';
                        }

                        $text .= '</span><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
                            <img style="position:absolute;top:'.$tagProp3.'in;left:3.46in;width:0.02in;height:0.39in" src="pdf/vi_17.png" />
                            <img style="position:absolute;top:'.$tagProp3.'in;left:2.61in;width:0.02in;height:0.39in" src="pdf/vi_18.png" />
                            <img style="position:absolute;top:'.$tagProp3.'in;left:0.75in;width:0.02in;height:0.39in" src="pdf/vi_19.png" />
                            <div style="position:absolute;top:'.$tagProp2.'in;left:-0.16in;width:0.53in;line-height:0.10in;"><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000">FACTURA</span><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
                            <div style="position:absolute;top:'.$tagProp2.'in;left:0.78in;width:0.80in;line-height:0.10in;"><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000">'. $imp->numDocSustento .'</span><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
                            <div style="position:absolute;top:'.$tagProp2.'in;left:2.87in;width:0.41in;line-height:0.10in;"><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000">'.$comprobante->infoCompRetencion->periodoFiscal.'</span><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>
                            <img style="position:absolute;top:'.$tagProp3.'in;left:1.59in;width:0.02in;height:0.39in" src="pdf/vi_20.png" />
                            <div style="position:absolute;top:'.$tagProp2.'in;left:1.7in;width:0.56in;line-height:0.10in;"><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000">'. $imp->fechaEmisionDocSustento .'</span><span style="font-style:normal;font-weight:normal;font-size:7pt;font-family:Helvetica;color:#000000"> </span><br/></SPAN></div>';
                        if((count($comprobante->impuestos->impuesto)-$temp)>0){
                            $text .= '<img  style="position:absolute;top:'.$tagProp.'in;left:-0.3in;width:8in;height:0.39in" src="pdf/vi_21.png" />
                            <img style="position:absolute;top:'.$tagProp.'in;left:4.82in;width:0.02in;height:0.39in" src="pdf/vi_22.png" />
                            <img style="position:absolute;top:'.$tagProp.'in;left:6.05in;width:0.02in;height:0.39in" src="pdf/vi_23.png" />
                            <img style="position:absolute;top:'.$tagProp.'in;left:6.98in;width:0.02in;height:0.39in" src="pdf/vi_24.png" />';
                        }
                        $temp=$temp+1;
                        $temp2=$temp2+1;
                        $temp3=$temp3+1;
                    }
                }

                $pdfFileName =str_replace(".xml",".pdf",$name);
                $pdfPath=$pdfDestinationPath . $pdfFileName;
                $pdf = PDF::loadView( 'pdf', compact('comprobante','text'))->setPaper('a4', 'portrait');

                Storage::put($pdfFileName, $pdf->output());
            }
            $pdfPathFile = "https://hcdarosita.com/pdfFile/" . $pdfFileName;

            $correoCC="";

            if(is_null($correo)){
                $correoCC="jorgeespinosaretenciones@gmail.com";
                $correo="jorgeespinosaretenciones@gmail.com";
            }
            else{
                $correoCC="jorgeespinosaretenciones@gmail.com";
            }

            Mail::send('sendmail',
            array(
                'name' => $name,
                'email' => $correo,
                'razon' => $razon,
                'fechaEmision' => $fecha,
                'emisor' => $emisor
            ), function($message) use ($pdfPathFile, $razon,$pathFile,$correoCC,$correo)
              {
               $message->from('docelec@hcdarosita.com','Hacienda Rosita');
               $message->to($correo, "$razon")->subject("Retenci贸n $razon");
               $message->cc($correoCC);
               $message->attach($pdfPathFile);
               $message->attach($pathFile);
           });

            $reSentCount = DB::table('files')->where('id', $id)->pluck('sentTimes');
            $rct=0;
            foreach ($reSentCount as $rc) {
                $rct=$rc;
            }
            // $count=0;
            // if(is_null($reSentCount)){
            //    $reSentCount=0;
            // }
            // echo $reSentCount;

            Storage::delete($pdfFileName);
            $now = Carbon::now('America/Bogota');

            DB::table('files')
            ->where('id', $id)
            ->update(['sent' => TRUE]);

            DB::table('files')
            ->where('id', $id)
            ->update(['sentTimes' => ($rct+1)]);


            DB::table('files')
            ->where('id', $id)
            ->update(['reSentDate' => $now->toDateString()]);

            DB::table('files')
            ->where('id', $id)
            ->update(['reSentTime' => $now->toTimeString()]);
        }
        return redirect("/home");
    }
}
