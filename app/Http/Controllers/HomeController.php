<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\File;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $filesNotSent = DB::table('files')
        ->orderBy('created_at', 'DESC')
        ->where('sent', FALSE)->get();

            $filesSent = DB::table('files')
            ->where('sent', TRUE)
            ->whereYear('created_at', '2019')->get();

        // if($files){
            // return view("admin.files.index",["filesNotSent"=>$filesNotSent,"filesSent"=>$filesSent]);
        // $files = File::All();
        // $files = DB::table('files')->where('sent', False)->get();
        return view('admin.home',["filesNotSent"=>$filesNotSent,"filesSent"=>$filesSent]);
    }
}
