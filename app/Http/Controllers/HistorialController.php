<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\File;

class HistorialController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function historial(){
        $filesSent = DB::table('files')
            ->where('sent', TRUE)
            ->whereYear('created_at', '2018')->get();
        return view('admin.history',["filesSent"=>$filesSent]);

    }


}